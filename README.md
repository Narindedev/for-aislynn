# For-Aislynn Sample Music Player Screen

## How to run the sample

1. To run the mobile application, Goto the project root folder and type 
  
  ```
  expo start
  ```

## screenshots of the Working App 

![ScreenShot](https://gitlab.com/Narindedev/for-aislynn/-/raw/master/screenshots/Screenshot%202020-07-14%20at%202.51.12%20AM.png)

![ScreenShot](https://gitlab.com/Narindedev/for-aislynn/-/raw/master/screenshots/Screenshot%202020-07-14%20at%202.51.25%20AM.png)

![ScreenShot](https://gitlab.com/Narindedev/for-aislynn/-/raw/master/screenshots/Screenshot%202020-07-14%20at%202.51.40%20AM.png)

![ScreenShot](https://gitlab.com/Narindedev/for-aislynn/-/raw/master/screenshots/Screenshot%202020-07-14%20at%202.51.49%20AM.png)