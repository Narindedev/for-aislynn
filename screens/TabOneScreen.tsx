import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  ScrollView,
  Alert
} from 'react-native';

import MusicItem from './MusicItem';
import { TextNormal, TextBold, TextMainTitle, TextAuthorTitle, TextNormal18, Header, HeaderLeft } from '../components/StyledText';


export default function TabOneScreen() {

  var jsonData = [{
    "srNo": 1,
    "title": "Adam Denisov",
    "description": "A brief track description",
    "duration": "3:12",
    "isPlaying": false
  }, {
    "srNo": 2,
    "title": "Anete Skodova",
    "description": "A brief track description",
    "duration": "1:51",
    "isPlaying": false
  }, {
    "srNo": 3,
    "title": "Bernd Pfefferberg",
    "description": "A brief track description",
    "duration": "1:51",
    "isPlaying": false
  }, {
    "srNo": 4,
    "title": "Freddy Kauschke",
    "description": "A brief track description",
    "duration": "4:08",
    "isPlaying": false
  }, {
    "srNo": 5,
    "title": "Harmen Porter",
    "description": "A brief track description",
    "duration": "1:08",
    "isPlaying": false
  }, {
    "srNo": 6,
    "title": "Monika Derfflinger",
    "description": "A brief track description",
    "duration": "0:95",
    "isPlaying": false
  }, {
    "srNo": 7,
    "title": "Uche Ogbonna",
    "description": "A brief track description",
    "duration": "1:14",
    "isPlaying": false
  }, {
    "srNo": 8,
    "title": "Zikoranumma Nkechi",
    "description": "A brief track description",
    "duration": "2:08",
    "isPlaying": false
  }];

  const [todos, setPlay] = useState([]);

  setPlaying = id => {
    setPlay(
      todos.map(todo => {
        if (todo.srNo === id) {
          todo.isPlaying = !todo.isPlaying;
        } else {
          todo.isPlaying = false;
        }
        return todo;
      })
    );
  };

  useEffect(() => {
    console.log("Inside useEffect todos length " + todos.length);
    if (todos.length < 8) {
      setPlay([...todos, ...jsonData]);
    }
  })



  return (

    <ImageBackground style={styles.container} source={require('../assets/bg.jpg')}>

      <View style={styles.innerContainer}>

        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollContainerStyle}>
          <Header >
            <Image source={require('../assets/ic_rating.png')} style={styles.ratingStyle} />
          </Header>
          <HeaderLeft >
            <Image source={require('../assets/ic_piggy_bank.png')} style={styles.piggyBankStyle} />
          </HeaderLeft>
          <View style={styles.topBody}>
            <TextMainTitle>{'TITLE'}</TextMainTitle>
            <Image source={require('../assets/ic_author.png')} style={styles.ImgStyle} />
            <TextAuthorTitle>{'by Robert Kiyosaki'}</TextAuthorTitle>
            <TextNormal style={{ marginTop: 10, width: '90%' }} >{'Lorem Ipsum is simply dummied text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book.'}</TextNormal>
            <TouchableOpacity style={styles.listenBtn} >
              <TextNormal18 >{'LISTEN'}</TextNormal18>
            </TouchableOpacity>
          </View>

          {todos.map(item => (
            <MusicItem
              key={item.srNo}
              srNo={item.srNo}
              title={item.title}
              description={item.description}
              duration={item.duration}
              isPlaying={item.isPlaying}
              setPlaying={() => setPlaying(item.srNo)}
            />
          ))}
        </ScrollView>
      </View>

    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  innerContainer: {
    width: '90%',
  },
  scrollContainerStyle: {
    flexGrow: 1,
    marginTop: '10%',
    marginBottom: '20%',
    borderRadius: 2,
    backgroundColor: 'rgb(48,46,48)'
  },
  topBody: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  listenBtn: {
    marginTop: 20,
    marginBottom: 20,
    height: 40,
    width: '90%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    backgroundColor: 'rgb(255,255,255)'
  },
  ImgStyle: {
    marginTop: 15,
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 45,
    overflow: "hidden",
    width: 90,
    height: 90,
    alignSelf: 'center',
  },
  ratingStyle: {
    borderColor: "#000000",
    height: 18,
    alignSelf: 'center',
  },
  piggyBankStyle: {
    borderColor: "#000000",
    height: 65,
    width: 65,
    alignSelf: 'center',
  },
});