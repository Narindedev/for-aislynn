
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { TextNormal, TextBold, ColumnLeft, ColumnRight, ListItem } from '../components/StyledText';


export default function MusicItem(props) {
    return (
        <ListItem>
            <ColumnLeft>
                <TextNormal>{props.srNo}</TextNormal>
            </ColumnLeft>
            <ColumnRight >
                <TextBold>{props.title}</TextBold>
                <TextNormal>{props.description}</TextNormal>
            </ColumnRight>
            <ColumnLeft>
                <TextNormal>{props.duration}</TextNormal>
            </ColumnLeft>
            <ColumnLeft>
                <Icon
                    name={props.isPlaying ? 'pause-circle' : 'play-circle'}
                    size={30}
                    color="black"
                    style={{}}
                    onPress={props.setPlaying}
                />
            </ColumnLeft>
        </ListItem >
    );
}

