import * as React from 'react';

import { Text, TextProps, View } from './Themed';

export function MonoText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />;
}

export function TextNormal(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 14, color: 'rgb(184,184,184)' }]} />;
}

export function TextBold(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 16, fontWeight: 'bold', color: 'rgb(236,236,236)' }]} />;
}

export function TextNormal18(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 18, color: 'black' }]} />;
}

export function TextAuthorTitle(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 20, fontWeight: 'bold', color: 'white', marginTop: 10, }]} />;
}

export function TextMainTitle(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 26, fontWeight: 'bold', color: 'white', marginTop: 35, }]} />;
}

export function ColumnLeft(props: TextProps) {
  return <View {...props} style={[{ width: '15%', alignItems: 'center', backgroundColor: 'rgb(48,46,48)' }]} />;
}

export function ColumnRight(props: TextProps) {
  return <View {...props} style={[{ width: '55%', flexDirection: "column", backgroundColor: 'rgb(48,46,48)' }]} />;
}

export function ListItem(props: TextProps) {
  return <View {...props} style={[{ flexDirection: 'row', width: '100%', alignItems: 'stretch', minHeight: 70, backgroundColor: 'rgb(48,46,48)' }]} />;
}

export function Header(props: TextProps) {
  return <View {...props} style={[{ right: 0, position: 'absolute', height: 40, justifyContent: 'center', marginRight: 15, backgroundColor: 'rgb(48,46,48)' }]} />;
}

export function HeaderLeft(props: TextProps) {
  return <View {...props} style={[{ left: 0, position: 'absolute', height: 70, justifyContent: 'center', marginRight: 15, backgroundColor: 'rgb(48,46,48)' }]} />;
}

export function FullScreenBg(props: TextProps) {
  return <View {...props} style={[{ left: 0, position: 'absolute', height: 70, justifyContent: 'center', marginRight: 15, backgroundColor: 'rgb(48,46,48)' }]} />;
}

